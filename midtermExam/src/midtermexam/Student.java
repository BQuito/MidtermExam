/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package midtermexam;

/**
 *
 * @author staff
 */
public class Student {
    
    private String name;
    private String surName;
    private String studID;

    public Student() {
    }

    public Student(String name, String surName, String studID) {
        this.name = name;
        this.surName = surName;
        this.studID = studID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getStudID() {
        return studID;
    }

    public void setStudID(String studID) {
        this.studID = studID;
    }

    @Override
    public String toString() {
        return String.format("Name:%s\nsurName:%s\nstudID:%s\n\n\n ", name,surName,studID);
    }
    
    
}
